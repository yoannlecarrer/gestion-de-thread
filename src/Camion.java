class Camion extends Thread{

	private Site[] sites;
	private int i = 0;
	private int camion_stock = 5;
	public static boolean isCamion = false;
	
	public Camion(Site[] sites) {
        this.sites = sites;
        //this.setDaemon(true); cette solution n'a pas march� 
    }
	
	public void run() {
		while(i<=sites.length && !Thread.interrupted()) {
			System.out.println("camion est arriv� "+i);
			isCamion=true;
			int stock_site = sites[i].getStock();
			
			//stock > 8 
			if(stock_site>=8) {
				
				int retirer_v�lo = stock_site - 5  ;
				retirerV�lo(sites[i],retirer_v�lo);
				i++;
				System.out.println("camion se d�place "+i);
				isCamion=false;
				try { Thread.sleep(500); } catch(InterruptedException e) {}
				
			//stock > 2
			}else if(stock_site<=2) {
				
				int ajouter_v�lo =  Site.STOCK_INIT - stock_site;
				ajouterV�lo(sites[i],ajouter_v�lo);
				System.out.println("camion se d�place "+i);
				i++;
				isCamion=false;
				try { Thread.sleep(500); } catch(InterruptedException e) {}
				
			}else {
				i++;
				isCamion=false;
				System.out.println("Le camion n'a rien a faire");
				System.out.println("camion se d�place "+i);
			}
			
			if (i==sites.length) {
				i=0;
				System.out.println("Restard camion");
				System.out.println("camion se d�place "+i);
			}
			
		}
		
	}
	
	public void ajouterV�lo(Site site,int nbV�lo) {
		int diff�rence = camion_stock - nbV�lo;
		System.out.println("diff�rence "+diff�rence);
		if ( diff�rence <= 0 ) {
			site.d�poserV�lo(camion_stock);
			System.out.println("Camion stock "+camion_stock+" d�pose "+camion_stock+" v�lo");
			camion_stock = 0;
		}
		else if (diff�rence > 0) {
			camion_stock = camion_stock - nbV�lo;
			site.d�poserV�lo(nbV�lo);
			System.out.println("Camion stock "+camion_stock+" d�pose "+nbV�lo+" v�lo");
		}
	}
	
	public  void retirerV�lo(Site site, int nbV�lo) {
		camion_stock = camion_stock + nbV�lo;
		site.prendreV�lo(nbV�lo);
		System.out.println("Camion stock "+camion_stock+" retire "+camion_stock+" v�lo");
	}
	
}
