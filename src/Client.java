
class Client extends Thread {
	
	private Site siteDepId ;
	private Site siteArrId;
	private int distanceSite;
	
	public Client(Site siteDepId, Site siteArrId) {
        this.siteDepId = siteDepId;
        this.siteArrId = siteArrId;	
    }
	
	/**
     * On emprunte sur le site de d�part si il y a plus de 0 v�lo
     * Sinon on attend
     */
	
	public void emprunter() {
		if(!Camion.isCamion) {
			siteDepId.prendreV�lo(1);
		}
    }

    /**
     * On d�pose son v�lo sur le site d'arriv� si il y a moins de 10 v�los
     * Sinon on attend
     */
    
	
    // Il faut les rendres atomique
    public void restituer() {
    	if(!Camion.isCamion) {
    		siteArrId.d�poserV�lo(1);
    	}
    }
    
    public int getDistance(){
    	if (siteArrId.getId()>=siteDepId.getId()) {
    		distanceSite = siteArrId.getId()-siteDepId.getId();
		}else if (siteArrId.getId()<siteDepId.getId()) {
			 distanceSite = siteDepId.getId()-siteArrId.getId();
		}
		return distanceSite*1000;
    }
    
    public void run() {
    	//while(true) {
    		emprunter();
        	try { 
        		Thread.sleep(getDistance()); 
        	} catch(InterruptedException e) {
        		
        	}
    		restituer();
    	//}
    }
}
