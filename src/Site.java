class Site {

	/* Constantes associees au site */
	static final int STOCK_INIT = 5; 
	static final int STOCK_MAX = 10;
	static final int BORNE_SUP = 8; // si v�lo sup�rieur a 8 on prend l'�xedent
	static final int BORNE_INF = 2; // si v�lo inf�rieur a 2 on remplis jusqu'a 5 
	
	private int id;
	private int stock = STOCK_INIT ;

	public Site(int id) {
		this.id = id;
	}
	
	public synchronized void d�poserV�lo(int nbV�lo) {
		while(stock>=STOCK_MAX) {
    		try {
    			System.out.println(Thread.currentThread().getName()+" attend");
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
		stock += nbV�lo;
		System.out.println(Thread.currentThread().getName()+" d�pose "+nbV�lo+" sur le site"+id);
		System.out.println(Thread.currentThread().getName()+" "+stock+" v�lo restant sur le site "+id);
    	notifyAll();
    }
	
	public synchronized void prendreV�lo(int nbV�lo) {
		while(stock==0) {
    		try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
		stock -= nbV�lo;
		System.out.println(Thread.currentThread().getName()+" prend "+nbV�lo+" sur le site"+id);
		System.out.println(Thread.currentThread().getName()+" "+stock+" v�lo restant sur le site "+id);
    	notifyAll();
    }
	
	public int getStock() {
		return stock;
	}
	
	public int getId()
	{
		return this.id;
	}

	
}
